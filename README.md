
FYGO CODING TEST:
=====
***

FYGO CODING TEST:


There're 3 types of customer operations (with their affects on customer balance):
1. Transaction: Purchase (positive)
2. Transaction: Refund (negative)
3. Withdrawal (negative)Transactions data is provided by a 3rd party service via webhooks.

Each transaction is delivered to us by a single request (no batching).
Incoming transactions may be represented as objects

```json
{"user_id": int,
"transaction_id": int
"amount" Decimal,
"created": datetime}
```
("amount" will contain a negative value in case of refunds).Withdrawals are initialized by our customers.

Requirements:
Django 3.x (any modules, any data structures) API only, no need for UI* Repository is hosted on github/bitbucket/gitlab.
Please either make it public or provide access to "ogurtsov" for review.*
1. Customers may exchange their username and password for an authentication token*
2. Customers may see a paginated list of their operations*
3. Customers may see the available balance* Customers may request a withdrawal* Backend is ready to accept webhooks with transaction data (no need to implement authentication, just match the user_id from the transaction object with a user from our system)

Bonus points:* Provide deployment instructions for one of the major providers (GCP, AWS, Azure, Heroku, etc). No need for actual configuration scripts: 2-3 sentences is enough.* We expect that the assignment takes around 4 hours. Do as much as you can within this timeframe and provide a description of what else you'd like to do but was unable to due to limited time.

***


***
 About the SOLUTION:

1. Transactions :
    - Endpoint for transactions (get method)
    - = > http://127.0.0.1:8000/user_transactions/{user_id}

2. Authentification:
    1. via basic authentification  => ( user : myfirstname , password = MyfirstnameFamilyname)
    2. via token ( get the token =>  python manage.py drf_create_tokenremi)


3. Available balance:
    - balance =>  http://127.0.0.1:8000/current_balance/{user_id}

4.  Deployement and  QA:
    - it has been deployed at :    https://fygo007.herokuapp.com/
    - login:    https://fygo007.herokuapp.com/admin
    - Postman / VScode_thunder collection for API calls => QA_call_Fygo.json
    - for some privacy reasons the git heroku has been removed but the deployment process  would look  like  would  be the following for a contrinutor.
        1. download the Heroku sdk
        2. get the credentials
        3. normal git routine  ( new branch, commit , push, merge to master ...)
        4. to deploy => git push heroku master

5. What should be improved:
    - Add unit tests
    - CICD to implement  (linter / test / deployment)
    - pagination  not setup
    - small UI


***