from django.apps import AppConfig


class CashbackAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cashback_app'
