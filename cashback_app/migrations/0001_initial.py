# Generated by Django 3.2.3 on 2021-05-26 14:40

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('transaction_id', models.IntegerField(primary_key=True, serialize=False)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('user_id', models.IntegerField(unique=True)),
                ('amount', models.DecimalField(decimal_places=2, max_digits=5)),
            ],
            options={
                'ordering': ['created'],
            },
        ),
    ]
