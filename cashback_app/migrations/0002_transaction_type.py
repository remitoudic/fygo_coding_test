# Generated by Django 3.2.3 on 2021-05-26 15:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cashback_app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='transaction',
            name='Type',
            field=models.CharField(default='XXX', max_length=50),
        ),
    ]
