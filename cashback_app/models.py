from django.db import models
from django.contrib.auth.models import User

class Transaction(models.Model):
    transaction_id =  models.AutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    Type =  models.CharField( default='XXX', max_length=50)
    #user_id =models.ForeignKey(User,on_delete=models.CASCADE )
    user_id = models.IntegerField()
    amount = models.DecimalField(max_digits=5, decimal_places=2,)

    class Meta:
        ordering = ['created']
