from django.urls import path
from cashback_app import views
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path ('', views.landing_page),
    path('user_transactions/<int:user_ID>', views.User_transactions.as_view()),
    path('current_balance/<int:user_ID>', views.User_current_balance. as_view()),
    path('withdrawal', views.webhook_withdrawal_receiver),
    path('token/', obtain_auth_token, name='api_token_auth')

]
