#from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from cashback_app.models import Transaction
from cashback_app.serializers import TransactionSerializer
from django.views.decorators.http import require_POST
from rest_framework.views import APIView
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
import json
from rest_framework.decorators import api_view


class User_transactions(APIView):
    """
    - get methode for  transactions history from one user
    - post methode for transaction  purchase(positiv) and refund (negative)
    """
    def get(self, request,user_ID ,format=None):
        transactions = Transaction.objects.filter(user_id=user_ID)
        serializer = TransactionSerializer(transactions, many=True)
        return JsonResponse(serializer.data, safe=False)

    def post(self, request,user_ID ,format=None):
        data = JSONParser().parse(request)
        serializer = TransactionSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=201)
        return JsonResponse(serializer.errors, status=400)

class User_current_balance(APIView):
    """
    Calculation of current balance for a user
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    def get (self, request,user_ID, format=None ):
        transactions_user= Transaction.objects.filter(user_id=user_ID)
        user_balance =sum([transac.amount for transac in  transactions_user])
        return JsonResponse({"balance":user_balance,
                              "user_ID": user_ID }, status=201)

@csrf_exempt
@require_POST
def webhook_withdrawal_receiver(request):
    jsondata = request.body
    data = json.loads(jsondata)
    for answer in data.items(): # go through all the answers
      print(answer) # print value of answers
    return HttpResponse(status=200)

@api_view(['GET','POST'])
@csrf_exempt
def landing_page(request):
    html = "<h1> FYGO coding test </h1>"
    return HttpResponse(html)