FROM python:3
ENV PYTHONUNBUFFERED=1
RUN mkdir C
WORKDIR /fygo_project
ADD . /fygo_project
RUN pip install -r requirements.txt